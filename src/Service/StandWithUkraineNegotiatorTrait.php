<?php

namespace Drupal\standwithukraine\Service;

use Drupal\service\ServiceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the StandWithUkraine negotiator service.
 */
trait StandWithUkraineNegotiatorTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceStandWithUkraineNegotiator = 'standwithukraine.negotiator';

  /**
   * Sets the StandWithUkraine negotiator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addStandWithUkraineNegotiator(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the StandWithUkraine negotiator.
   */
  protected function standWithUkraineNegotiator(): StandWithUkraineNegotiatorInterface {
    return $this->getKnownService();
  }

}
