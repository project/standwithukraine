<?php

namespace Drupal\standwithukraine\Service;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Defines the image service interface.
 */
interface StandWithUkraineImageInterface extends TrustedCallbackInterface {

  /**
   * The cached data identifiers prefix.
   */
  public const string CACHE = 'standwithukraine:';

  /**
   * The flag colors.
   */
  public const array COLORS = [
    TRUE => [0x00, 0x5B, 0xBB],
    FALSE => [0xFF, 0xD5, 0x00],
  ];

  /**
   * The default ratio for an unknown theme.
   *
   * The first half of the space is for blue color and the second half space for
   * yellow color.
   */
  public const int UNDEFINED_RATIO = 50;

  /**
   * The category of supported media types.
   *
   * This text can not be added to the following constant directly because in
   * that case, Drupal Rector will define array items in that constant as
   * libraries.
   */
  public const string CATEGORY = 'image/';

  /**
   * The image filters.
   */
  public const array FILTERS = [
    FALSE => 'foreground',
    TRUE => 'background',
  ];

  /**
   * The supported media types.
   */
  public const array TYPES = [
    'png' => [
      self::CATEGORY . 'png',
    ],
    'ico' => [
      self::CATEGORY . 'vnd.microsoft.icon',
      self::CATEGORY . 'x-icon',
    ],
  ];

  /**
   * The supported location of files.
   */
  public const string STREAM = 'public://';

  /**
   * Adds fields to form to configure ratio and filling type.
   *
   * @param int $ratio
   *   The default ratio.
   * @param bool $background
   *   TRUE, if the background should be styled instead of the foreground.
   *
   * @return array
   *   The renderable element.
   */
  public function form(int $ratio, bool $background): array;

  /**
   * Checks if a file type is one from the list of processable.
   *
   * @param string $type
   *   The MIME type.
   */
  public function isSupported(string $type): bool;

  /**
   * Looking for images among renderable elements to modify them.
   *
   * @param array $elements
   *   The renderable elements tree.
   * @param array $variables
   *   The block theme variables.
   * @param int $ratio
   *   The percentage of flag first color.
   * @param bool $background
   *   TRUE, if the background should be styled instead of the foreground.
   * @param string|null $path
   *   (optional) The location of files. Defaults to NULL.
   * @param string|null $template
   *   (optional) The pattern of the main part of the file name. Defaults to
   *   NULL.
   * @param string|null $theme
   *   (optional) The theme name. Defaults to NULL.
   */
  public function search(
    array &$elements,
    array $variables,
    int $ratio,
    bool $background,
    ?string $path = NULL,
    ?string $template = NULL,
    ?string $theme = NULL,
  ): void;

}
