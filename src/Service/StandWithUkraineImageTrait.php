<?php

namespace Drupal\standwithukraine\Service;

use Drupal\service\ServiceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the StandWithUkraine image service.
 */
trait StandWithUkraineImageTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceStandWithUkraineImage = 'standwithukraine.image';

  /**
   * Sets the StandWithUkraine image.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addStandWithUkraineImage(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the StandWithUkraine image.
   */
  protected function standWithUkraineImage(): StandWithUkraineImageInterface {
    return $this->getKnownService();
  }

}
