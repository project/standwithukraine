<?php

namespace Drupal\standwithukraine\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Elphin\IcoFileLoader\IcoFileService;

/**
 * Defines the image service.
 */
class StandWithUkraineImage implements StandWithUkraineImageInterface {

  use CompositeFormElementTrait;
  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * StandWithUkraineImage constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\StreamWrapper\PublicStream $stream
   *   The public stream wrapper.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme
   *   The theme manager.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator.
   */
  public function __construct(
    protected CacheBackendInterface $cache,
    LoggerChannelFactoryInterface $logger_factory,
    protected PublicStream $stream,
    TranslationInterface $translation,
    protected ThemeManagerInterface $theme,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
  ) {
    $this
      ->setLoggerFactory($logger_factory)
      ->setStringTranslation($translation);
  }

  /**
   * Gets cached data.
   *
   * @param string $root
   *   The main part of result files.
   * @param string $theme
   *   The theme name.
   */
  protected function cached(string $root, string $theme): ?array {
    if (($cache = $this->cache->get(static::CACHE . $root)) !== FALSE) {
      $items = $cache->data;
    }

    return isset($items[$theme]) ? $items : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function form(int $ratio, bool $background): array {
    return [
      'ratio' => [
        '#type' => 'number',
        '#title' => $this->t('Ratio'),
        '#description' => $this->t('Define how many percentages of icon height should be dedicated to blue color.'),
        '#min' => 10,
        '#max' => 90,
        '#default_value' => $ratio,
        '#field_suffix' => '%',
      ],
      'type' => [
        '#type' => 'radios',
        '#title' => $this->t('Type'),
        '#options' => [
          'foreground' => $this->t('Foreground'),
          'background' => $this->t('Background'),
        ],
        '#default_value' => static::FILTERS[$background],
        '#pre_render' => [
          [static::class, 'preRenderCompositeFormElement'],
          [static::class, 'preRenderField'],
        ],
      ],
    ];
  }

  /**
   * Re-styling images in Ukraine flag colors.
   *
   * @param string $theme
   *   The theme name.
   * @param string $filename
   *   The filename.
   * @param string $root
   *   The main part of result files.
   * @param int $ratio
   *   The percentage of flag first color.
   * @param bool $background
   *   TRUE, if the background should be styled instead of the foreground.
   * @param bool $cache
   *   (optional) FALSE, if files shouldn't be tried to get from the cache.
   *   Defaults to TRUE.
   *
   * @return array
   *   An array of arrays. Each key is the theme name (e.g. seven), and each
   *   value is an array of paths to images, keyed by dimensions (e.g. 32x32).
   */
  protected function get(
    string $theme,
    string $filename,
    string $root,
    int $ratio,
    bool $background,
    bool $cache = TRUE,
  ): array {
    if (($items = $cache ? $this->cached($root, $theme) : NULL) !== NULL) {
      return $items;
    }

    $items = [];

    if (!preg_match(
      '/\.(' . implode('|', array_keys(static::TYPES)) . ')$/',
      $filename,
    )) {
      $this->cache->set(static::CACHE . $root, $items);

      return $items;
    }

    $loader = new IcoFileService();

    try {
      $images = $loader->from(DRUPAL_ROOT . DIRECTORY_SEPARATOR . $filename);
    }
    catch (\Exception $exception) {
      $this->getLogger('standwithukraine')->warning($exception->getMessage());

      return $items;
    }

    foreach ($images as $image) {
      $width = $image->width;
      $height = $image->height;
      $middle = $height / 100 * $ratio;
      $image = $loader->renderImage($image);
      $unique_alpha = $alpha_matrix = [];

      for ($vertical = $height - 1; $vertical >= 0; $vertical--) {
        for ($horizontal = 0; $horizontal < $width; $horizontal++) {
          $color_index = imagecolorat($image, $horizontal, $vertical);
          $rgba_color = imagecolorsforindex($image, $color_index);
          $alpha = $rgba_color['alpha'];
          $alpha_matrix[$horizontal][$vertical] = $alpha;

          if (!$background) {
            $unique_alpha[$vertical < $middle][$alpha] = 0;
          }
        }
      }

      for (
        $color_index = 0;
        $color_index < imagecolorstotal($image);
        $color_index++
      ) {
        imagecolordeallocate($image, $color_index);
      }

      if (!$background) {
        foreach ($unique_alpha as $color_type => &$alpha_per_type) {
          foreach ($alpha_per_type as $alpha => &$color_index) {
            $color_index = call_user_func_array(
              'imagecolorallocatealpha',
              [$image, ...static::COLORS[$color_type], $alpha],
            );
          }

          unset($color_index);
        }
      }

      for ($vertical = $height - 1; $vertical >= 0; $vertical--) {
        for ($horizontal = 0; $horizontal < $width; $horizontal++) {
          $alpha = $alpha_matrix[$horizontal][$vertical];
          $color_type = $vertical < $middle;

          if ($background) {
            if (!$alpha) {
              continue;
            }

            $color_index = imagecolorat($image, $horizontal, $vertical);
            $rgba_color = imagecolorsforindex($image, $color_index);

            unset($rgba_color['alpha']);

            $alpha /= 128;

            $rgb_color = array_map(
              fn(int $background_color, int $image_color): int => round(
                $image_color * (1 - $alpha) + $background_color * $alpha,
              ),
              static::COLORS[$color_type],
              array_values($rgba_color),
            );

            $color_index = call_user_func_array(
              'imagecolorallocatealpha',
              [$image, ...$rgb_color, 0],
            );
          }
          else {
            $color_index = $unique_alpha[$color_type][$alpha];
          }

          imagesetpixel($image, $horizontal, $vertical, $color_index);
        }
      }

      $filename = sprintf(
        static::STREAM . 'standwithukraine' . str_repeat('-%s', 3) . '.png',
        $root,
        $theme,
        $dimensions = "{$height}x$width",
      );

      try {
        imagepng($image, $filename);
      }
      catch (\Exception $exception) {
        $this->getLogger('standwithukraine')->warning($exception->getMessage());

        continue;
      }

      $items[$theme][$dimensions] = $this->fileUrlGenerator
        ->generateString($filename);
    }

    $this->cache->set(static::CACHE . $root, $items);

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function isSupported(string $type): bool {
    return isset(static::TYPES[$type]);
  }

  /**
   * Describes both types of favicon styling.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   element.
   */
  public static function preRenderField(array $element): array {
    $element['foreground']['#description'] = t('Change colors of the visible part of images and leave the transparent background as is.');
    $element['background']['#description'] = t('Add the Ukraine flag behind of image visible part and leave the second one as is.');
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function search(
    array &$elements,
    array $variables,
    int $ratio,
    bool $background,
    ?string $path = NULL,
    ?string $template = NULL,
    ?string $theme = NULL,
  ): void {
    if ($path === NULL) {
      $path = preg_quote($this->stream->basePath(), '#');
    }

    if ($template === NULL) {
      $template = 'block' . str_repeat('-%s', 3);
    }

    if ($theme === NULL) {
      $theme = $this->theme->getActiveTheme()->getName();
    }

    $pattern = sprintf(
      '#^(%s|%s%s).+\.([^.]+)$#',
      static::STREAM,
      DIRECTORY_SEPARATOR,
      $path,
    );

    foreach (Element::children($elements) as $key) {
      $element = &$elements[$key];

      if (
        isset($element['#theme']) &&
        $element['#theme'] === 'image' &&
        !empty($element['#uri']) &&
        preg_match($pattern, $element['#uri'], $matches) &&
        $this->isSupported($matches[2])
      ) {
        $destination = sprintf(
          $template,
          $variables['base_plugin_id'],
          $variables['plugin_id'],
          $key,
        );

        $items = $this->get(
          $theme,
          str_replace(
            static::STREAM,
            $path . DIRECTORY_SEPARATOR,
            $element['#uri'],
          ),
          $destination,
          $ratio,
          $background,
        );

        if (!empty($items[$theme])) {
          $element['#uri'] = sprintf(
            '%s?v=%u%s',
            current($items[$theme]),
            $ratio,
            static::FILTERS[$background][0],
          );
        }
      }
      elseif (
        isset($element['#type']) &&
        $element['#type'] === 'link' &&
        !empty($element['#title']) &&
        is_array($element['#title'])
      ) {
        $this->search(
          $element['#title'],
          $variables,
          $ratio,
          $background,
          $path,
          $template,
          $theme,
        );
      }

      unset($element);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRenderCompositeFormElement', 'preRenderField'];
  }

}
