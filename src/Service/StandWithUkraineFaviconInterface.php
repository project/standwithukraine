<?php

namespace Drupal\standwithukraine\Service;

/**
 * Defines the favicon service interface.
 */
interface StandWithUkraineFaviconInterface extends StandWithUkraineImageInterface {

  /**
   * The default ratio for a known theme.
   *
   * Use a specific ratio for themes that are provided by Drupal core or someone
   * else who is using favicon in Drupal logo style (image of drop).
   */
  public const int DEFINED_RATIO = 60;

  /**
   * The predefined themes list.
   */
  public const array THEMES = [
    'bartik',
    'bootstrap',
    'claro',
    'gin',
    'olivero',
    'seven',
    'stark',
  ];

  /**
   * Extracts images from favicon and style them in Ukraine flag colors.
   *
   * @param string $theme
   *   The theme name.
   *
   * @return array
   *   An array of arrays. Each key is the theme name (e.g. seven), and each
   *   value is an array of paths to images, keyed by dimensions (e.g. 32x32).
   */
  public function build(string $theme): array;

}
