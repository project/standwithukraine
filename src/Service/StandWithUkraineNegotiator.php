<?php

namespace Drupal\standwithukraine\Service;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\standwithukraine\StandWithUkraineSettingsInterface;

/**
 * Provides a class that overrides message settings.
 *
 * It therefore uses StandWithUkraineNegotiatorInterface objects which are
 * passed in using the 'standwithukraine_negotiator' tag.
 */
class StandWithUkraineNegotiator implements StandWithUkraineNegotiatorInterface {

  /**
   * Constructs a new StandWithUkraineNegotiator.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   * @param string[] $negotiators
   *   An array of negotiator identifiers.
   */
  public function __construct(
    protected ClassResolverInterface $classResolver,
    protected array $negotiators = [],
  ) {}

  /**
   * {@inheritdoc}
   */
  public function applies(StandWithUkraineSettingsInterface $settings): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function override(StandWithUkraineSettingsInterface $settings): void {
    foreach ($this->negotiators as $negotiator_id) {
      $negotiator = $this->classResolver
        ->getInstanceFromDefinition($negotiator_id);

      if ($negotiator->applies($settings)) {
        $negotiator->override($settings);
      }
    }
  }

}
