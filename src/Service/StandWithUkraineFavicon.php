<?php

namespace Drupal\standwithukraine\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * Defines the favicon service.
 */
class StandWithUkraineFavicon extends StandWithUkraineImage implements StandWithUkraineFaviconInterface {

  /**
   * StandWithUkraineFavicon constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\StreamWrapper\PublicStream $stream
   *   The public stream wrapper.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme
   *   The theme manager.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   */
  public function __construct(
    protected CacheBackendInterface $cache,
    LoggerChannelFactoryInterface $logger_factory,
    protected PublicStream $stream,
    TranslationInterface $translation,
    protected ThemeManagerInterface $theme,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
    protected ConfigFactoryInterface $configFactory,
  ) {
    call_user_func_array(
      [parent::class, __FUNCTION__],
      array_slice(func_get_args(), 0, 6),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(string $theme): array {
    if (
      ($items = $this->cached($root = 'favicon', $theme)) !== NULL ||
      ($filename = theme_get_setting("$root.url", $theme)) === NULL
    ) {
      return $items;
    }

    $config = $this->configFactory->get('standwithukraine.settings');
    $settings = (array) $config->get("$root.themes.$theme");

    return $this->get(
      $theme,
      $filename,
      $root,
      $settings['ratio'] ?? static::UNDEFINED_RATIO,
      !empty($settings['background']),
      FALSE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isSupported(string $type): bool {
    foreach (static::TYPES as $types) {
      if (in_array($type, $types)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
