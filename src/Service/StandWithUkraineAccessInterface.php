<?php

namespace Drupal\standwithukraine\Service;

/**
 * Defines the access service interface.
 */
interface StandWithUkraineAccessInterface {

  /**
   * Checks if the module feature should be available for the current user.
   *
   * @param string $permission
   *   The name of permission for a specific feature.
   */
  public function isAllowed(string $permission): bool;

}
