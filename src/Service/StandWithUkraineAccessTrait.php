<?php

namespace Drupal\standwithukraine\Service;

use Drupal\service\ServiceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the StandWithUkraine access service.
 */
trait StandWithUkraineAccessTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceStandWithUkraineAccess = 'standwithukraine.access';

  /**
   * Sets the StandWithUkraine access.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addStandWithUkraineAccess(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the StandWithUkraine access.
   */
  protected function standWithUkraineAccess(): StandWithUkraineAccessInterface {
    return $this->getKnownService();
  }

}
