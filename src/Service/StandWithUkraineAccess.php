<?php

namespace Drupal\standwithukraine\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\RoleInterface;

/**
 * Defines the access service.
 */
class StandWithUkraineAccess implements StandWithUkraineAccessInterface {

  /**
   * StandWithUkraineAccess constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current active user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected AccountProxyInterface $currentUser,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function isAllowed(string $permission): bool {
    foreach ($this->currentUser->getRoles() as $role_id) {
      $role = $this->entityTypeManager->getStorage('user_role')->load($role_id);

      if (
        $role instanceof RoleInterface &&
        !$role->isAdmin() &&
        (
          $role->hasPermission('disable standwithukraine features') ||
          $role->hasPermission($permission)
        )
      ) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
