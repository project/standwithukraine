<?php

namespace Drupal\standwithukraine\Service;

use Drupal\service\ServiceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the StandWithUkraine favicon service.
 */
trait StandWithUkraineFaviconTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceStandWithUkraineFavicon = 'standwithukraine.favicon';

  /**
   * Sets the StandWithUkraine favicon.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addStandWithUkraineFavicon(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the StandWithUkraine favicon.
   */
  protected function standWithUkraineFavicon(): StandWithUkraineFaviconInterface {
    return $this->getKnownService();
  }

}
