<?php

namespace Drupal\standwithukraine\Service;

use Drupal\standwithukraine\StandWithUkraineSettingsInterface;

/**
 * Defines an interface for classes that overrides message settings.
 */
interface StandWithUkraineNegotiatorInterface {

  /**
   * Whether this message negotiator should be used to override settings.
   *
   * @param \Drupal\standwithukraine\StandWithUkraineSettingsInterface $settings
   *   The settings object.
   *
   * @return bool
   *   TRUE if this negotiator should be used or FALSE to let other negotiators
   *   decide.
   */
  public function applies(StandWithUkraineSettingsInterface $settings): bool;

  /**
   * Overrides settings.
   *
   * @param \Drupal\standwithukraine\StandWithUkraineSettingsInterface $settings
   *   The settings object.
   */
  public function override(StandWithUkraineSettingsInterface $settings): void;

}
