<?php

namespace Drupal\standwithukraine;

use Drupal\Core\GeneratedLink;
use Drupal\Core\Link;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\Url;
use Drupal\service\RendererTrait;

/**
 * Provides link field alter.
 */
trait StandWithUkraineTrait {

  use CompositeFormElementTrait;
  use RendererTrait;

  /**
   * The suggested websites list.
   */
  protected array $urls = [
    'https://supportukrainenow.org',
    'https://war.ukraine.ua/',
  ];

  /**
   * Adds link field to form.
   *
   * @param array $form
   *   The extendable form.
   * @param array $values
   *   The default values.
   */
  public function alter(array &$form, array $values): void {
    $description = $this->t('Take into account the following rules:');

    $element = [
      '#theme' => 'item_list',
      '#items' => [
        $this->t('Empty lines will be deleted and other lines will be combined when their number is more than two.'),
        $this->t(
          'When a field is empty then a message will contain the following phrase - %text.',
          [
            '%text' => str_replace(
              PHP_EOL,
              ' ',
              StandWithUkraineSettingsInterface::TEXT,
            ),
          ]
        ),
      ],
    ];

    $description .= $this->renderer()->render($element);

    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#description' => $description,
      '#rows' => 2,
      '#resizable' => 'horizontal',
      '#default_value' => $values['text'] ?? '',
    ];

    $form['pair'] = [
      '#type' => 'radios',
      '#title' => $this->t('Lines number'),
      '#description' => $this->t("Define if the message's text should be split or displayed as a single row."),
      '#options' => [$this->t('One'), $this->t('Two')],
      '#default_value' => (int) ($values['pair'] ?? 0),
    ];

    $form['sizes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Bigger font size'),
      '#description' => $this->t('This may be necessary if you want to increase the text size of one of the lines to bring its width closer to the width of the other.'),
      '#options' => [
        'first' => $this->t('First line'),
        'last' => $this->t('Second line'),
      ],
      '#default_value' => array_keys(
        array_filter((array) ($values['sizes'] ?? [])),
      ),
      '#pre_render' => [
        [static::class, 'preRenderCompositeFormElement'],
        [static::class, 'preRenderField'],
      ],
    ];

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Link'),
      '#description' => sprintf(
        '%s %s.',
        $this->t('For example:'),
        implode(
          ', ',
          array_map(
            fn(string $url): GeneratedLink => Link::fromTextAndUrl(
              parse_url($url, PHP_URL_HOST),
              Url::fromUri($url, ['target' => '_blank']),
            )->toString(),
            $this->urls,
          ),
        ),
      ),
      '#default_value' => $values['url'] ?? '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRenderCompositeFormElement', 'preRenderField'];
  }

  /**
   * Configures font sizes for each line.
   *
   * Describe when font size can be changed for specific lines. Also, disable
   * the font size option of the second line when a single-line view is used.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   element.
   */
  public static function preRenderField(array $element): array {
    $element['first']['#description'] = t('Available when a message is displayed in the single-line view or double-line view.');
    $element['last']['#description'] = t('Available only when a message is displayed in the double-line view.');

    $element['last']['#states'] = [
      'disabled' => [
        ':input[name="pair"]' => [
          'value' => '0',
        ],
      ],
    ];

    return $element;
  }

}
