<?php

namespace Drupal\standwithukraine\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\service\BlockBase;
use Drupal\service\ConfigFactoryTrait;
use Drupal\standwithukraine\StandWithUkraineTrait;

/**
 * Provides a block to display a message on the background of the flag.
 */
#[Block(
  id: 'standwithukraine_block',
  admin_label: new TranslatableMarkup('StandWithUkraine'),
)]
class StandWithUkraineBlock extends BlockBase implements StandWithUkraineBlockInterface {

  use ConfigFactoryTrait;
  use StandWithUkraineTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this->addConfigFactory()->addRenderer();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return static::CONFIGURATION + [
      'inherit' => array_fill_keys(
        array_keys(static::CONFIGURATION),
        static::INHERIT,
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $link = Link::createFromRoute(
      $this->t('the settings page'),
      'standwithukraine.settings',
      options: [
        'attributes' => [
          'target' => '_blank',
        ],
      ]
    );

    $form['inherit'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Inherit'),
      '#description' => $this->t(
        'Re-use value from the field on the @url where this field has the same title as the checkbox of the current field.',
        ['@url' => $link->toString()],
      ),
      '#default_value' => array_keys(
        array_filter($this->configuration['inherit']),
      ),
    ];

    $this->alter($form, $this->configuration);

    foreach (array_keys(static::CONFIGURATION) as $name) {
      $form[$name]['#states'] = [
        'disabled' => [
          ":input[name=\"settings[inherit][$name]\"]" => [
            'checked' => TRUE,
          ],
        ],
      ];

      $form['inherit']['#options'][$name] = $form[$name]['#title'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    foreach (['inherit', ...array_keys(static::CONFIGURATION)] as $key) {
      $this->configuration[$key] = $form_state->getValue($key);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $config = $this->configFactory()->get('standwithukraine.settings');
    $options = [];

    foreach (static::CONFIGURATION as $key => $default_value) {
      $global_value = $config->get("message.$key");

      $options["#$key"] = $this->configuration['inherit'][$key] &&
        ($default_value !== NULL || !empty($global_value))
        ? $global_value : $this->configuration[$key];
    }

    $options['#single'] = empty($options['#pair']);

    unset($options['#pair']);

    return [
      '#theme' => 'standwithukraine',
      '#attached' => [
        'library' => ['standwithukraine/common'],
      ],
    ] + $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), ['standwithukraine']);
  }

}
