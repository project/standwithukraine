<?php

namespace Drupal\standwithukraine\Plugin\Block;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides an interface for a block to display a message.
 */
interface StandWithUkraineBlockInterface extends TrustedCallbackInterface {

  /**
   * The default value for inheriting options.
   */
  public const bool INHERIT = TRUE;

  /**
   * The default values.
   */
  public const array CONFIGURATION = [
    'text' => NULL,
    'pair' => FALSE,
    'sizes' => [
      'first' => FALSE,
      'last' => TRUE,
    ],
    'url' => NULL,
  ];

}
