<?php

namespace Drupal\standwithukraine;

use Drupal\block\BlockInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait as CoreStringTranslationTrait;
use Drupal\Core\Template\Attribute;
use Drupal\service\ClassResolverBase;
use Drupal\service\ConfigFactoryTrait;
use Drupal\service\EntityTypeManagerTrait;
use Drupal\service\ModuleHandlerTrait;
use Drupal\service\RendererTrait;
use Drupal\service\RouteMatchTrait;
use Drupal\service\StringTranslationTrait;
use Drupal\service\ThemeManagerTrait;
use Drupal\standwithukraine\Plugin\Block\StandWithUkraineBlockInterface;
use Drupal\standwithukraine\Service\StandWithUkraineAccessTrait;
use Drupal\standwithukraine\Service\StandWithUkraineFaviconTrait;
use Drupal\standwithukraine\Service\StandWithUkraineImageInterface;
use Drupal\standwithukraine\Service\StandWithUkraineImageTrait;
use Drupal\standwithukraine\Service\StandWithUkraineNegotiatorTrait;

/**
 * Provides hook wrappers.
 *
 * @internal
 *   This is an internal utility class wrapping hook implementations.
 */
class StandWithUkraineBuilder extends ClassResolverBase {

  use ConfigFactoryTrait;
  use CoreStringTranslationTrait;
  use EntityTypeManagerTrait;
  use ModuleHandlerTrait;
  use RendererTrait;
  use RouteMatchTrait;
  use StandWithUkraineAccessTrait {
    standWithUkraineAccess as access;
  }
  use StandWithUkraineFaviconTrait {
    standWithUkraineFavicon as favicon;
  }
  use StandWithUkraineImageTrait {
    standWithUkraineImage as image;
  }
  use StandWithUkraineNegotiatorTrait {
    standWithUkraineNegotiator as negotiator;
  }
  use StringTranslationTrait {
    StringTranslationTrait::getStringTranslation insteadof CoreStringTranslationTrait;
  }
  use ThemeManagerTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this
      ->addConfigFactory()
      ->addEntityTypeManager()
      ->addModuleHandler()
      ->addRenderer()
      ->addRouteMatch()
      ->addStandWithUkraineAccess()
      ->addStandWithUkraineFavicon()
      ->addStandWithUkraineImage()
      ->addStandWithUkraineNegotiator()
      ->addStringTranslation()
      ->addThemeManager();
  }

  /**
   * Alter the result of \Drupal\Core\Block\BlockBase::build().
   *
   * @param array &$build
   *   A renderable array of data, only containing #cache.
   * @param \Drupal\Core\Block\BlockPluginInterface $block
   *   The block plugin instance.
   *
   * @see standwithukraine_block_build_alter()
   */
  public function alterBlockBuild(
    array &$build,
    BlockPluginInterface $block,
  ): void {
    $count = $this->entityTypeManager()->getStorage('block')
      ->getQuery()
      ->condition('status', TRUE)
      ->condition('plugin', $block->getBaseId())
      ->condition('third_party_settings.standwithukraine.enabled', TRUE)
      ->count()
      ->accessCheck(FALSE)
      ->execute();

    if ($count > 0) {
      $build['#cache']['contexts'][] = 'user.permissions';
    }
  }

  /**
   * Provide a form-specific alteration instead of the global hook_form_alter().
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself.
   *
   * @see standwithukraine_form_block_form_alter()
   */
  public function alterBlockForm(
    array &$form,
    FormStateInterface $form_state,
    string $form_id,
  ): void {
    if ($form['settings']['provider']['#value'] === 'standwithukraine') {
      return;
    }

    $form_object = $form_state->getFormObject();

    if (
      !$form_object instanceof EntityFormInterface ||
      !($block = $form_object->getEntity()) instanceof BlockInterface
    ) {
      return;
    }

    $settings = $block->getThirdPartySettings('standwithukraine');

    $subform = [
      '#type' => 'details',
      '#title' => 'Stand With Ukraine 🇺🇦',
      '#open' => !empty($settings['enabled']),
    ];

    $subform['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Apply a filter to all images inside this block to display them in the colors of the Ukraine flag.'),
      '#default_value' => $subform['#open'],
    ];

    $fields = $this->image()->form(
      $settings['ratio'] ?? StandWithUkraineImageInterface::UNDEFINED_RATIO,
      !empty($settings['background']),
    );

    $fields['background'] = $fields['type'];

    unset($fields['type']);

    foreach ($fields as &$field) {
      $field['#states'] = [
        'enabled' => [
          ':input[name="third_party_settings[standwithukraine][enabled]"]' => [
            'checked' => TRUE,
          ],
        ],
      ];
    }

    $form['third_party_settings']['standwithukraine'] = $subform + $fields;

    $form['actions']['submit']['#submit'] = [
      [static::class, 'submit'],
      ...($form['actions']['submit']['#submit'] ?? []),
    ];

    if (isset($form['third_party_settings']['#weight'])) {
      return;
    }

    if (isset($form['settings']['#weight'])) {
      $weight = $form['settings']['#weight'] + 1;
    }
    elseif (isset($form['visibility']['#weight'])) {
      $weight = $form['visibility']['#weight'] - 1;
    }
    else {
      $weight = 0;

      foreach (Element::children($form) as $key) {
        if ($key !== 'third_party_settings') {
          $form[$key]['#weight'] = $weight++;

          if ($key === 'settings') {
            $weight++;
          }
        }
      }

      $weight = $form['settings']['#weight'] + 1;
    }

    if (isset($weight)) {
      $form['third_party_settings']['#weight'] = $weight;
    }
  }

  /**
   * Converts value of image filter type.
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function submit(
    array $form,
    FormStateInterface $form_state,
  ): void {
    $key = ['third_party_settings', 'standwithukraine'];

    foreach ($form_state->getValue($key) as $setting_name => $setting_value) {
      $parents = [...$key, $setting_name, '#default_value'];

      if (NestedArray::getValue($form, $parents) != $setting_value) {
        drupal_flush_all_caches();
        break;
      }
    }

    $key[] = 'background';

    $form_state->setValue($key, $form_state->getValue($key) === end($key));
  }

  /**
   * Provide online user help.
   *
   * @param string $route_name
   *   For page-specific help, use the route name as identified in the module's
   *   routing.yml file.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @see standwithukraine_help()
   */
  public function help(
    string $route_name,
    RouteMatchInterface $route_match,
  ): string {
    return match ($route_name) {
      'help.page.standwithukraine' => <<<HTML
<h3>{$this->t('About')}</h3>
<p>{$this->t('The StandWithUkraine 🇺🇦 module provides a message and styles images to show your support for Ukrainians.')}</p>

<h3>{$this->t('Uses')}</h3>
<dl>
  <dt>{$this->t('Message')}</dt>
  <dd>{$this->t('Message on the background of the flag which can be attached to one of the three sides of the page (top, right, left).')}</dd>
  <dt>{$this->t('Flag style')}</dt>
  <dd>{$this->t('Painting the favicon and images from blocks in the colors of the Ukraine flag.')}</dd>
  <dt>{$this->t('Block')}</dt>
  <dd>{$this->t('A block contains a message provided by the first feature.')}</dd>
</dl>
HTML,
      'standwithukraine.settings' => "<p>{$this->t('Configure how a message should look and where it should be placed, and override the favicon.')}</p>",
      default => '',
    };
  }

  /**
   * Add attachments (typically assets) to a page before it is rendered.
   *
   * @param array &$attachments
   *   An array that you can add attachments to.
   *
   * @see standwithukraine_page_attachments()
   */
  public function pageAttachments(array &$attachments): void {
    if (!$this->access()->isAllowed('hide standwithukraine message')) {
      return;
    }

    $pair = $this->configFactory()->get('standwithukraine.settings')
      ->get('message.pair');

    $settings = new StandWithUkraineSettings(empty($pair));

    $this->negotiator()->override($settings);

    $height = $settings->isSingle() ? 2.5 : 4;
    $color = implode(', ', StandWithUkraineImageInterface::COLORS[TRUE]);

    $attachments['#attached']['html_head'][] = [
      [
        '#type' => 'html_tag',
        '#tag' => 'style',
        '#value' => <<<CSS
:root {
  --standwithukraine-height: {$height}em;
  --standwithukraine-height-extra: {$height}rem;
  --standwithukraine-color-blue: rgb($color);
}
CSS,
      ],
      'standwithukraine_style',
    ];
  }

  /**
   * Alter attachments (typically assets) to a page before it is rendered.
   *
   * @param array &$attachments
   *   Array of all attachments provided by hook_page_attachments()
   *   implementations.
   *
   * @see standwithukraine_page_attachments_alter()
   */
  public function alterPageAttachments(array &$attachments): void {
    $attached = &$attachments['#attached'];
    $found = FALSE;

    foreach ($attached['html_head'] as $element) {
      if (
        is_array($element) &&
        isset($element[1]) &&
        $element[1] === 'standwithukraine_style'
      ) {
        $found = TRUE;
        break;
      }
    }

    if ($found) {
      $defaults = [
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#attributes' => [
          'name' => 'theme-color',
          'content' => NULL,
        ],
      ];

      $found = FALSE;

      $color = 'rgb(' .
        implode(', ', StandWithUkraineImageInterface::COLORS[TRUE]) . ')';

      foreach ($attached['html_head'] as &$element) {
        if (is_array($element) && !empty($element[0])) {
          $sub_element = &$element[0];
          $broken = FALSE;

          foreach ($defaults as $key => $value) {
            if (isset($sub_element[$key])) {
              if (is_array($value)) {
                if (is_array($sub_element[$key])) {
                  foreach ($value as $sub_key => $sub_value) {
                    if (isset($sub_element[$key][$sub_key])) {
                      if (
                        $sub_value !== NULL &&
                        $sub_element[$key][$sub_key] !== $sub_value
                      ) {
                        $broken = TRUE;
                      }
                    }
                    else {
                      $broken = TRUE;
                    }
                  }

                  if ($broken) {
                    break 2;
                  }
                }
                else {
                  $broken = TRUE;
                }
              }
              elseif ($sub_element[$key] !== $value) {
                $broken = TRUE;
              }
            }
            else {
              $broken = TRUE;
            }

            if ($broken) {
              break;
            }
          }

          if (!$broken) {
            $sub_element['#attributes']['content'] = $color;
            $found = TRUE;
          }
        }
      }

      if (!$found) {
        foreach (['light', 'dark'] as $type) {
          $defaults['#attributes']['media'] = "(prefers-color-scheme: $type)";
          $defaults['#attributes']['content'] = $color;

          $attached['html_head'][] = [
            $defaults,
            "standwithukraine_theme_$type",
          ];
        }
      }
    }

    if (
      !theme_get_setting('features.favicon') ||
      !isset($attached['html_head_link']) ||
      !is_array($attached['html_head_link']) ||
      $this->configFactory()->get('standwithukraine.settings')
        ->get('favicon.disable') ||
      !$this->access()->isAllowed('standwithukraine keep favicon') ||
      !$this->favicon()->isSupported(theme_get_setting('favicon.mimetype'))
    ) {
      return;
    }

    $groups = &$attached['html_head_link'];
    $keys = [];

    unset($element);

    foreach ($groups as $group_id => $group) {
      if (is_array($group)) {
        foreach ($group as $element_id => $element) {
          if (
            is_array($element) &&
            isset($element['rel'], $element['href'], $element['type']) &&
            in_array('icon', explode(' ', $element['rel'])) &&
            $this->favicon()->isSupported($element['type'])
          ) {
            $keys[$element['sizes'] ?? 'any'] = [$group_id, $element_id];
          }
        }
      }
    }

    if (empty($keys)) {
      return;
    }

    $theme = $this->themeManager()->getActiveTheme()->getName();
    $config = $this->configFactory()->get('system.theme');

    if ($config->get('favicon') && $config->get('admin') === $theme) {
      $theme = $config->get('default');
    }

    $urls = $this->favicon()->build($theme);

    if (isset($urls[$theme])) {
      $urls = $urls[$theme];

      uksort($urls, function () {
        $sizes = array_map(
          fn(string $dimensions): string
            => substr($dimensions, 0, strpos($dimensions, 'x')),
          func_get_args(),
        );

        return $sizes[0] <=> $sizes[1];
      });

      if (!isset($urls[$dimensions = $default_dimensions = '16x16'])) {
        $dimensions = key($urls);
      }

      $type = current(current(StandWithUkraineImageInterface::TYPES));

      $link = [
        'href' => $urls[$dimensions],
        'type' => $type,
        'sizes' => $dimensions,
      ];

      unset($urls[$dimensions]);

      $selected_dimensions = isset($keys[$default_dimensions])
        ? $default_dimensions : 'any';

      foreach ($link as $field => $value) {
        NestedArray::setValue(
          $groups,
          [...$keys[$selected_dimensions], $field],
          $value,
        );
      }

      foreach (['', 'apple-touch-'] as $prefix) {
        $link = ['rel' => "{$prefix}icon"];

        if (empty($prefix)) {
          $link['type'] = $type;
        }

        foreach ($urls as $dimensions => $url) {
          NestedArray::setValue(
            $groups,
            $keys[$dimensions] ?? [count($groups), 0],
            $link + [
              'href' => $url,
              'sizes' => $dimensions,
            ],
          );

          if (isset($keys[$dimensions])) {
            unset($keys[$dimensions]);
          }
        }
      }
    }
  }

  /**
   * Add a renderable array to the top of the page.
   *
   * @param array $page_top
   *   A renderable array representing the top of the page.
   *
   * @see standwithukraine_page_top()
   */
  public function pageTop(array &$page_top): void {
    if (!$this->access()->isAllowed('hide standwithukraine message')) {
      return;
    }

    $config = $this->configFactory()->get('standwithukraine.settings');
    $options = (array) $config->get('message');

    $settings = new StandWithUkraineSettings();

    if (array_key_exists('position', $options)) {
      $settings->setPosition(Position::from((string) $options['position']));
    }

    if (isset($options['offset'])) {
      $settings->setOffset($options['offset']);
    }

    $this->negotiator()->override($settings);

    $attributes = ['class' => ['simple']];

    if (($position = $settings->getPosition()) !== Position::Top) {
      $attributes['class'][] = $position->value;

      if (!empty($offset = $settings->getOffset())) {
        $attributes['style'] = "top: {$offset}px";
      }

      $library = 'base';
    }
    else {
      $library = ($theme = $this->themeManager()->getActiveTheme())->getName();
      $parents = $theme->getBaseThemeExtensions();

      foreach ($names = ['socialblue', 'bootstrap'] as $name) {
        if ($library === $name) {
          break;
        }
        elseif (isset($parents[$name])) {
          $library = $name;

          break;
        }
      }

      if (!in_array($library, $names)) {
        $library = 'base';
      }
    }

    $element = [
      '#theme' => 'standwithukraine',
      '#attributes' => $attributes,
      '#attached' => [
        'library' => ["standwithukraine/$library"],
      ],
    ];

    foreach (array_keys(StandWithUkraineBlockInterface::CONFIGURATION) as $key) {
      if (isset($options[$key])) {
        $element["#$key"] = $options[$key];
      }
    }

    if (array_key_exists('#pair', $element)) {
      if (!empty($element['#pair'])) {
        $element['#single'] = FALSE;
      }

      unset($element['#pair']);
    }

    if (
      $this->routeMatch()->getRouteName() !== 'standwithukraine.settings' &&
      $this->moduleHandler()->moduleExists('contextual')
    ) {
      if (!empty($options['url'])) {
        $element['#attached']['library'][] = 'standwithukraine/contextual';
      }

      $element['#contextual'] = [
        '#contextual_links' => [
          'standwithukraine' => [
            'route_parameters' => [],
          ],
        ],
      ];
    }

    $this->renderer()->addCacheableDependency($element, $config);

    $page_top['standwithukraine'] = $element;
  }

  /**
   * Preprocess theme variables for a specific theme hook.
   *
   * @param array $variables
   *   The variables array (modify in place).
   *
   * @see standwithukraine_preprocess_block()
   */
  public function preprocessBlock(array &$variables): void {
    if (
      !isset(
        $variables['base_plugin_id'],
        $variables['plugin_id'],
        $variables['elements']['#id'],
      ) ||
      $variables['base_plugin_id'] === 'standwithukraine_block' ||
      !$this->access()->isAllowed('standwithukraine keep blocks')
    ) {
      return;
    }

    $block = $this->entityTypeManager()->getStorage('block')
      ->load($variables['elements']['#id']);

    if ($block instanceof BlockInterface) {
      $settings = $block->getThirdPartySettings('standwithukraine');

      if (!empty($settings['enabled'])) {
        $this->image()->search(
          $variables['content'],
          $variables,
          $settings['ratio'] ?? StandWithUkraineImageInterface::UNDEFINED_RATIO,
          !empty($settings['background']),
        );
      }
    }
  }

  /**
   * Prepares variables for standwithukraine element templates.
   *
   * @param array $variables
   *   An associative array.
   *
   * @see template_preprocess_standwithukraine()
   */
  public function preprocessTemplate(array &$variables): void {
    $attributes = (new Attribute($variables['attributes']))
      ->addClass('standwithukraine');

    if (!empty($variables['url'])) {
      $variables['tag'] = 'a';

      $attributes
        ->setAttribute('href', $variables['url'])
        ->setAttribute('target', '_blank');
    }

    $settings = (new StandWithUkraineSettings($variables['single']))
      ->setText($variables['text'])
      ->setSizes(...$variables['sizes']);

    $this->negotiator()->override($settings);

    if (empty($lines = $settings->getText())) {
      $lines = StandWithUkraineSettingsInterface::TEXT;
    }

    $lines = explode(PHP_EOL, $lines);

    if (($pair = $settings->isPair()) && count($lines) === 1) {
      $lines = str_word_count($lines[0], 1);
      $lines = array_chunk($lines, round(count($lines) / 2));

      array_walk($lines, function (array|string &$line): void {
        $line = implode(' ', $line);
      });
    }

    $lines = [
      'first' => $lines[0],
      'last' => $lines[1] ?? '',
    ];

    $keys = array_keys($lines);

    if ($pair) {
      $attributes->addClass('double');
    }
    else {
      $lines = [implode(' ', $lines)];
      $attributes->addClass('single');
    }

    $sizes = $settings->getSizes();

    $variables['lines'] = array_map(
      function (int|string $position, string $text) use ($keys, $sizes) {
        $line_attributes = (new Attribute())->addClass('line');

        if (is_string($position)) {
          $line_attributes->addClass($position);
        }

        static $delta = 0;

        if ($delta === 1 || ($sizes[$keys[$delta]] ?? FALSE)) {
          $line_attributes->addClass('bigger');
        }

        $delta++;

        return [
          'attributes' => $line_attributes,
          'content' => $text,
        ];
      },
      array_keys($lines),
      array_values($lines),
    );

    $variables['attributes'] = $attributes;
  }

}
