<?php

namespace Drupal\standwithukraine\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\service\ConfigFactoryTrait;
use Drupal\service\ControllerBase;
use Drupal\standwithukraine\Service\StandWithUkraineFaviconTrait;

/**
 * Controller for standwithukraine pages.
 */
class StandWithUkraineController extends ControllerBase {

  use ConfigFactoryTrait;
  use StandWithUkraineFaviconTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this->addConfigFactory()->addStandWithUkraineFavicon();
  }

  /**
   * Checks access for the favicon settings page.
   */
  public function access(): AccessResultInterface {
    $config = $this->config('system.theme');

    foreach (['default', 'admin'] as $type) {
      if (!empty($name = $config->get($type))) {
        $favicon = $this->standWithUkraineFavicon()->build($name);

        if (isset($favicon[$name])) {
          return AccessResult::allowed();
        }
      }
    }

    return AccessResult::neutral();
  }

}
