<?php

namespace Drupal\standwithukraine;

/**
 * Defines an interface for class for message settings.
 */
interface StandWithUkraineSettingsInterface {

  /**
   * The message phrase is by default.
   */
  public const string TEXT = 'We Stand' . PHP_EOL . 'With Ukraine';

  /**
   * Returns text phrase.
   */
  public function getText(): string;

  /**
   * Defines text phrase.
   *
   * @param string $text
   *   The text phrase.
   */
  public function setText(string $text): static;

  /**
   * Checks if the message's text should be displayed in two lines.
   */
  public function isPair(): bool;

  /**
   * Checks if the message's text should be displayed in one line.
   */
  public function isSingle(): bool;

  /**
   * Displays message in view mode of two lines.
   */
  public function setDouble(): static;

  /**
   * Returns a font size of each message line.
   */
  public function getSizes(): array;

  /**
   * Defines which type of font size should be used for each message line.
   *
   * @param bool $first
   *   TRUE if the top message line should use a bigger font size.
   * @param bool $last
   *   TRUE if the bottom message line should use a bigger font size.
   */
  public function setSizes(bool $first, bool $last): static;

  /**
   * Returns the page side where a message should be displayed.
   */
  public function getPosition(): Position;

  /**
   * Defines the page side where a message should be displayed.
   *
   * @param \Drupal\standwithukraine\Position $position
   *   The position type.
   */
  public function setPosition(Position $position): static;

  /**
   * Returns message offset in pixels.
   */
  public function getOffset(): int;

  /**
   * Defines message offset.
   *
   * @param int $offset
   *   The number of pixels.
   */
  public function setOffset(int $offset): static;

}
