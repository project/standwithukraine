<?php

namespace Drupal\standwithukraine\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\service\TimeTrait;
use Drupal\standwithukraine\Service\StandWithUkraineFaviconInterface;
use Drupal\standwithukraine\Service\StandWithUkraineFaviconTrait;
use Drupal\standwithukraine\Service\StandWithUkraineImageInterface;

/**
 * Configure StandWithUkraine favicon settings for this site.
 */
class StandWithUkraineFaviconForm extends StandWithUkraineFormBase {

  use StandWithUkraineFaviconTrait;
  use TimeTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return parent::creation()->addStandWithUkraineFavicon()->addTime();
  }

  /**
   * {@inheritdoc}
   */
  protected function getName(): string {
    return 'favicon';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $this->config = $this->config($this->getEditableConfigNames()[0]);

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Activate functionality for styling favicon.'),
      '#default_value' => !$this->read('disable'),
    ];

    $form['themes'] = [
      '#type' => 'details',
      '#title' => $this->t('Themes'),
      '#description' => $this->t('Configure favicon for default theme or admin theme.'),
      '#tree' => TRUE,
      '#open' => FALSE,
      '#states' => [
        'disabled' => [
          ':input[name="favicon[enable]"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    $theme_config = $this->config('system.theme');
    $themes = (array) $this->read('themes');
    $suffix = '?timestamp=' . $this->time()->getRequestTime();

    /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $title */
    foreach ([$this->t('Default'), $this->t('Admin')] as $title) {
      $theme_type = strtolower($title->getUntranslatedString());
      $theme_name = $theme_config->get($theme_type);

      if (empty($theme_name)) {
        continue;
      }

      $favicon = $this->standWithUkraineFavicon()->build($theme_name);

      if (!isset($favicon[$theme_name])) {
        continue;
      }

      if (isset($themes[$theme_name])) {
        $ratio = (int) $themes[$theme_name]['ratio'];
      }
      elseif (in_array($theme_name, StandWithUkraineFaviconInterface::THEMES)) {
        $ratio = StandWithUkraineFaviconInterface::DEFINED_RATIO;
      }
      else {
        $ratio = StandWithUkraineImageInterface::UNDEFINED_RATIO;
      }

      $custom = $ratio !== StandWithUkraineImageInterface::UNDEFINED_RATIO;

      $form['themes']['#open'] |= $custom;

      $form['themes'][$theme_name] = [
        '#type' => 'details',
        '#title' => $title,
        '#open' => $custom,
      ];

      $form['themes'][$theme_name]['preview'] = [
        '#theme' => 'table',
        '#caption' => $this->t('Preview'),
        '#attributes' => [
          'class' => ['preview'],
        ],
        '#attached' => [
          'library' => ['standwithukraine/form'],
        ],
      ];

      $rows = &$form['themes'][$theme_name]['preview']['#rows'];

      foreach ($favicon[$theme_name] as $dimensions => $url) {
        $rows['images'][]['data'] = [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'src' => "$url$suffix",
          ],
        ];

        $rows['dimensions'][]['data'] = $dimensions;
      }

      if (($columns = count($rows['images'])) > 1) {
        for (
          $current_column = 0;
          $current_column < $columns - 1;
          $current_column++
        ) {
          for (
            $next_column = $current_column + 1;
            $next_column < $columns;
            $next_column++
          ) {
            $sizes = array_map(
              fn(int $column): string => explode(
                'x',
                $rows['dimensions'][$column]['data'],
              )[0],
              [$current_column, $next_column],
            );

            if ($sizes[0] > $sizes[1]) {
              foreach (array_keys($rows) as $row) {
                $buffer = $rows[$row][$current_column];
                $rows[$row][$current_column] = $rows[$row][$next_column];
                $rows[$row][$next_column] = $buffer;
              }
            }
          }
        }
      }

      $form['themes'][$theme_name] += $this->standWithUkraineFavicon()->form(
        $ratio,
        !empty($themes[$theme_name]['background']),
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    ($config = $this->config($this->getEditableConfigNames()[0]))
      ->set('favicon.disable', !$form_state->getValue('enable'));

    foreach (Element::children($form['themes']) as $theme_name) {
      $path = ['themes', $theme_name];
      $name = 'favicon.' . implode('.', $path) . '.';

      $config
        ->set("{$name}ratio", $form_state->getValue([...$path, 'ratio']))
        ->set(
          "{$name}background",
          array_search(
            $form_state->getValue([...$path, 'type']),
            StandWithUkraineImageInterface::FILTERS,
          ),
        );
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
