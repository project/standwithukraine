<?php

namespace Drupal\standwithukraine\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\service\CacheTagsInvalidatorTrait;
use Drupal\service\EntityTypeManagerTrait;
use Drupal\standwithukraine\Position;

/**
 * Configure StandWithUkraine message settings for this site.
 */
class StandWithUkraineMessageForm extends StandWithUkraineFormBase {

  use CacheTagsInvalidatorTrait;
  use EntityTypeManagerTrait;

  /**
   * The list of fields that are used in both elements.
   */
  protected array $base;

  /**
   * The list of fields that are used in the message element only.
   */
  protected array $all;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return parent::creation()
      ->addCacheTagsInvalidator()
      ->addEntityTypeManager();
  }

  /**
   * {@inheritdoc}
   */
  protected function getName(): string {
    return 'message';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $this->config = $this->config($this->getEditableConfigNames()[0]);

    $this->alter($form, (array) $this->config->get($this->getName()));
    $this->base = Element::children($form);

    $form['position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Position'),
      '#description' => $this->t('Select on which side of the page a message should be displayed.'),
      '#default_value' => $this->read('position') ?? Position::Top->value,
    ];

    $form['offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Offset'),
      '#description' => $this->t('How many pixels down the message should be shifted down the page? When a field is empty then the message will be shifted down by 20 percent from the page height. Available only for the left or right position of the message.'),
      '#min' => 0,
      '#field_suffix' => $this->t('pixels'),
      '#default_value' => $this->read('offset'),
    ];

    foreach (Position::cases() as $position) {
      $label = $this->t($position->name);

      if ($position === Position::Top) {
        $label .= " ({$this->t('default')})";
      }
      else {
        $form['offset']['#states']['enabled'][':input[name="position"]'][] = [
          'value' => $position->value,
        ];
      }

      $form['position']['#options'][$position->value] = $label;
    }

    $this->all = Element::children($form);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $values = $form_state->getValues();

    foreach ($this->base as $key) {
      if ($config->get($key) != $values[$key]) {
        $count = $this->entityTypeManager()->getStorage('block')
          ->getQuery()
          ->condition('status', TRUE)
          ->condition('plugin', 'standwithukraine_block')
          ->condition('settings.inherit.*', TRUE)
          ->count()
          ->accessCheck(FALSE)
          ->execute();

        if ($count > 0) {
          $this->cacheTagsInvalidator()->invalidateTags(['standwithukraine']);
        }

        break;
      }
    }

    $lines = &$values['text'];
    $lines = explode(PHP_EOL, $lines);

    array_walk($lines, function (string &$line): void {
      $line = trim($line);
    });

    if (($count = count($lines)) > 2) {
      $lines = array_chunk($lines, round($count / 2));

      array_walk($lines, function (array|string &$line): void {
        $line = implode(' ', $line);
      });
    }

    $lines = implode(PHP_EOL, array_filter($lines));

    foreach ($this->all as $key) {
      $config->set("{$this->getName()}.$key", $values[$key]);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
