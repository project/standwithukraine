<?php

namespace Drupal\standwithukraine\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\service\CacheTrait;
use Drupal\service\ConfigFormBase;
use Drupal\standwithukraine\Service\StandWithUkraineImageInterface;
use Drupal\standwithukraine\StandWithUkraineTrait;

/**
 * Configure StandWithUkraine settings for this site.
 */
abstract class StandWithUkraineFormBase extends ConfigFormBase implements TrustedCallbackInterface {

  use CacheTrait;
  use StandWithUkraineTrait;

  /**
   * The config.
   */
  protected Config $config;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this->addCache()->addRenderer();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['standwithukraine.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "standwithukraine_{$this->getName()}_form";
  }

  /**
   * Returns form identifier central part and configuration record prefix.
   */
  abstract protected function getName(): string;

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $this->cache()
      ->invalidate(StandWithUkraineImageInterface::CACHE . 'favicon');

    parent::submitForm($form, $form_state);
  }

  /**
   * Gets data from configuration object.
   *
   * @param string $key
   *   A string that maps to a key within the configuration data.
   */
  protected function read(string $key): mixed {
    return $this->config->get("{$this->getName()}.$key");
  }

}
