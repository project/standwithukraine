<?php

declare(strict_types=1);

namespace Drupal\standwithukraine;

/**
 * Enum for supported position types.
 *
 * Sticky a message to one of the following side of the page.
 */
enum Position: string {

  case Left = 'left';
  case Top = '';
  case Right = 'right';

}
