<?php

namespace Drupal\standwithukraine;

use Drupal\block\BlockInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\standwithukraine\Plugin\Block\StandWithUkraineBlockInterface;

/**
 * Provides a BC layer for modules providing old configurations.
 *
 * @internal
 *   This class is only meant to fix outdated standwithukraine configuration and
 *   its methods should not be invoked directly.
 */
class StandWithUkraineConfigUpdater extends ConfigEntityUpdater {

  /**
   * Initiates block entities update.
   *
   * @param array $sandbox
   *   Stores information for batch updates.
   * @param string $function
   *   The post update hook function name.
   */
  public function action(array &$sandbox, string $function): void {
    if (($divider = strrpos($function, '_')) !== FALSE) {
      $method = substr($function, $divider + 1);

      $this->update(
        $sandbox,
        'block',
        function(BlockInterface $block) use ($method): bool {
          if ($block->getPluginId() === 'standwithukraine_block') {
            $settings = $block->get('settings');

            $this->$method($settings);

            $block->set('settings', $settings)->save();

            return TRUE;
          }

          return FALSE;
        },
      );
    }
  }

  /**
   * Inherit any block option from module settings.
   *
   * @param array $settings
   *   The block settings.
   *
   * @see standwithukraine_post_update_inherit()
   */
  protected function inherit(array &$settings): void {
    $settings['inherit'] = [
      'single' => StandWithUkraineBlockInterface::INHERIT,
      'url' => $settings['inherit'],
    ];
  }

  /**
   * Change the option for the number of lines for blocks.
   *
   * @param array $settings
   *   The block settings.
   *
   * @see standwithukraine_post_update_single()
   */
  protected function single(array &$settings): void {
    foreach ([[], ['inherit']] as $delta => $prefix) {
      $parents = [...$prefix, 'single'];
      $state = NestedArray::getValue($settings, $parents) == $delta;
      NestedArray::setValue($settings, [...$prefix, 'pair'], $state);
      NestedArray::unsetValue($settings, $parents);
    }
  }

}
