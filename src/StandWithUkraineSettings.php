<?php

namespace Drupal\standwithukraine;

/**
 * Provides a class for message settings.
 */
class StandWithUkraineSettings implements StandWithUkraineSettingsInterface {

  /**
   * Constructs a new StandWithUkraineSettings.
   *
   * @param bool $single
   *   (optional) Display the message's text as a single line. Defaults to
   *   FALSE.
   * @param \Drupal\standwithukraine\Position $position
   *   (optional) The page side where a message should be displayed. Defaults to
   *   Position::Top.
   * @param int $offset
   *   (optional) The message offset. Defaults to 0.
   * @param string $text
   *   (optional) The text phrase. Defaults to
   *   StandWithUkraineSettingsInterface::TEXT.
   * @param array $sizes
   *   (optional) The font sizes list. Defaults to an empty array.
   */
  public function __construct(
    protected bool $single = FALSE,
    protected Position $position = Position::Top,
    protected int $offset = 0,
    protected string $text = self::TEXT,
    protected array $sizes = [],
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getText(): string {
    return $this->text;
  }

  /**
   * {@inheritdoc}
   */
  public function setText(string $text): static {
    $this->text = $text;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPair(): bool {
    return !$this->isSingle();
  }

  /**
   * {@inheritdoc}
   */
  public function isSingle(): bool {
    return $this->single;
  }

  /**
   * {@inheritdoc}
   */
  public function setDouble(): static {
    $this->single = FALSE;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSizes(): array {
    return $this->sizes;
  }

  /**
   * {@inheritdoc}
   */
  public function setSizes(bool $first, bool $last): static {
    $this->sizes = [
      'first' => $first,
      'last' => $last,
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPosition(): Position {
    return $this->position;
  }

  /**
   * {@inheritdoc}
   */
  public function setPosition(Position $position): static {
    $this->position = $position;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOffset(): int {
    return $this->offset;
  }

  /**
   * {@inheritdoc}
   */
  public function setOffset(int $offset): static {
    $this->offset = $offset;

    return $this;
  }

}
