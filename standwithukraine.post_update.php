<?php

/**
 * @file
 * Post update functions for StandWithUkraine 🇺🇦.
 */

use Drupal\standwithukraine\StandWithUkraineConfigUpdater;

/**
 * Inherit any block option from module settings.
 */
function standwithukraine_post_update_inherit(array &$sandbox): void {
  \Drupal::classResolver(StandWithUkraineConfigUpdater::class)
    ->action($sandbox, __FUNCTION__);
}

/**
 * Change the option for the number of lines for blocks.
 */
function standwithukraine_post_update_single(array &$sandbox): void {
  \Drupal::classResolver(StandWithUkraineConfigUpdater::class)
    ->action($sandbox, __FUNCTION__);
}
