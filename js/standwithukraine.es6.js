(function ($, Drupal) {
  Drupal.behaviors.standWithUkraine = {
    attach(context) {
      let clicked = false;

      $(document).on('drupalContextualLinkAdded', () => {
        const $element = $('.standwithukraine.simple', context);

        $element.on('click', () => {
          if (clicked) {
            clicked = false;
            return false;
          }
        });

        $element.find('.contextual').on('click', () => {
          clicked = true;
        });

        $element.find('.contextual a', context).on('click', () => {
          clicked = true;
          location.href = $(this).attr('href');
        });
      });
    }
  }
})(jQuery, Drupal);
