(function ($, Drupal) {
  Drupal.behaviors.standWithUkraine = {
    attach: function attach(context) {
      var clicked = false;

      $(document).on('drupalContextualLinkAdded', function () {
        var $element = $('.standwithukraine.simple', context);

        $element.on('click', function () {
          if (clicked) {
            clicked = false;
            return false;
          }
        });

        $element.find('.contextual').on('click', function () {
          clicked = true;
        });

        $element.find('.contextual a', context).on('click', function () {
          clicked = true;
          location.href = $(this).attr('href');
        });
      });
    }
  }
})(jQuery, Drupal);
