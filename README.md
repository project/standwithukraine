```
  ____  _                  ___        ___ _   _     _   _ _              _
 / ___|| |_ __ _ _ __   __| \ \      / (_) |_| |__ | | | | | ___ __ __ _(_)_ __   ___
 \___ \| __/ _` | '_ \ / _` |\ \ /\ / /| | __| '_ \| | | | |/ / '__/ _` | | '_ \ / _ \
  ___) | || (_| | | | | (_| | \ V  V / | | |_| | | | |_| |   <| | | (_| | | | | |  __/
 |____/ \__\__,_|_| |_|\__,_|  \_/\_/  |_|\__|_| |_|\___/|_|\_\_|  \__,_|_|_| |_|\___|
```

# StandWithUkraine 🇺🇦

The StandWithUkraine 🇺🇦 provides the following features to show your support for
Ukrainians:

1. Message on the background of the flag which can be attached to one of the
   three sides of the page (top, right, left).
1. Painting the favicon and images from blocks in the colors of the Ukraine
   flag.
1. A block contains a message provided by the first feature.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/standwithukraine).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/standwithukraine).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Service](https://www.drupal.org/project/service)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure the user permissions in Administration > People > Permissions:
  - *Administer StandWithUkraine*

    Users with this permission will configure module settings.

  - *Hide message, use original favicon and blocks*

    Users with this permission will not see the message and overridden favicon.

  - *Hide message*

    Users with this permission will not see the message.

  - *Use original favicon*

    Users with this permission will not see overridden favicon.

  - *Use original blocks*

    Users with this permission will not see overridden images of blocks.

- Navigate to Administration > Structure > Block layout > Configure and check
  the StandWithUkraine 🇺🇦 checkbox to apply a filter to block's images to
  display them in Ukraine flag colors.

- Customize the module settings in Administration > Configuration >
  User interface > StandWithUkraine 🇺🇦.


## Maintainers

- Oleksandr Horbatiuk - [lexhouk](https://www.drupal.org/u/lexhouk)
